import React from 'react';
import {Switch, Route} from 'react-router-dom';
import NoMatch from "./components/NoMatch";
import {LoginPage, NewPasswordPage, ResetPasswordPage, ProtectedPage} from "./components/Pages";

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/login" component={LoginPage}/>
            <Route exact path="/login/reset" component={ResetPasswordPage} />
            <Route path="/login/reset/password/:token" component={NewPasswordPage} />
            <Route path="/test" component={ProtectedPage}/>
            <Route path="*">
                <NoMatch />
            </Route>
        </Switch>
    );
};

export default Routes;
