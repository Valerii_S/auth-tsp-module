import axios from 'axios';

export default class TspServices {
    _baseUrl = 'http://localhost:9000';

    login = (email, password) => {
        return new Promise((resolve, reject) => {
            resolve(
                axios.post(`${this._baseUrl}/login`, {
                    email,
                    password,
                }).then(response => {
                    if (response.data.success) {
                        localStorage.setItem('userHash', response.data.token);
                        localStorage.setItem('login', response.data.email);
                    }
                })
            );
            reject(new Error('failed local login)'));
        })
    };

    loginSocial = (id, route, history) => {
        return new Promise((resolve, reject) => {
            resolve(
                axios.post(`${this._baseUrl}/login/${route}`, {
                    id
                })
                    .then(response => localStorage.setItem('userHash', response.data.token))
                    .then(() => history.push('/test'))
            );
            reject(new Error('failed login'));
        })
    };

    resetPassword = (email) => {
        return new Promise((resolve, reject) => {
            resolve(
                axios.post(`${this._baseUrl}/login/reset`, {
                    email,
                })
                    .then(response => localStorage.setItem('id', response.data))
            );
            reject(new Error('failed to rest password!'));
        });
    };

    logout = () => {
        return new Promise((resolve, reject) => {
            resolve(
                axios.get('http://localhost:9000/logout')
                    .then(response => {
                        if (response.data.success === true) {
                            localStorage.removeItem('userHash');
                            localStorage.removeItem('login', response.data.email);
                        }
                    })
            );
            reject(new Error('error on logout'));
        })
    };
}
