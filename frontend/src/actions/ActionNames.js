const LOGIN_REQUEST = 'LOGIN_REQUEST';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_FAILURE = 'LOGIN_FAILURE';

const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
const LOGOUT_FAILURE = 'LOGOUT_FAILURE';

const LOGIN_WITH_SOCIAL_REQUEST = 'LOGIN_WITH_SOCIAL_REQUEST';
const LOGIN_WITH_SOCIAL_SUCCESS = 'LOGIN_WITH_SOCIAL_SUCCESS';
const LOGIN_WITH_SOCIAL_FAILURE = 'LOGIN_WITH_SOCIAL_FAILURE';

const CLEAR_ALL_MESSAGES = 'CLEAR_ALL_MESSAGES';

const RESET_PASSWORD_REQUEST = 'RESET_PASSWORD_REQUEST';
const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';
const RESET_PASSWORD_FAILURE = 'RESET_PASSWORD_FAILURE';

export {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGIN_WITH_SOCIAL_REQUEST,
    LOGIN_WITH_SOCIAL_SUCCESS,
    LOGIN_WITH_SOCIAL_FAILURE,
    LOGOUT_SUCCESS,
    LOGOUT_REQUEST,
    LOGOUT_FAILURE,
    RESET_PASSWORD_REQUEST,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAILURE,
    CLEAR_ALL_MESSAGES
}