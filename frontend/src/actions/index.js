import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGIN_WITH_SOCIAL_REQUEST,
    LOGIN_WITH_SOCIAL_SUCCESS,
    LOGIN_WITH_SOCIAL_FAILURE,
    LOGOUT_SUCCESS,
    LOGOUT_REQUEST, RESET_PASSWORD_REQUEST, RESET_PASSWORD_SUCCESS, RESET_PASSWORD_FAILURE, CLEAR_ALL_MESSAGES
} from '../actions/ActionNames';

const loginLoaded = () => {
    return {
        type: LOGIN_REQUEST,
    }
};

const loginRequested = () => {
    return {
        type: LOGIN_SUCCESS,
    }
};

const loginError = (error) => {
    return {
        type: LOGIN_FAILURE,
        payload: error,
        messages: 'Неверный логин или пароль',
    }
};

const loginSocialLoaded = () => {
    return {
        type: LOGIN_WITH_SOCIAL_REQUEST,
    }
};

const loginSocialRequest = () => {
    return {
        type: LOGIN_WITH_SOCIAL_SUCCESS,
    }
};

const loginSocialError = (error) => {
    return {
        type: LOGIN_WITH_SOCIAL_FAILURE,
        payload: error,
        messages: 'Такого аккаунта не существует! Зарегистрируйтесь, Валера старался когда ее делал!',
    }
};

const resetPasswordRequest = () => {
    return {
        type: RESET_PASSWORD_SUCCESS,
    }
};

const resetPasswordLoaded = () => {
    return {
        type: RESET_PASSWORD_REQUEST,
        messagesSuccess: 'Спасибо что воспользовались формой! Провереьте свой @email на наличие письма',
    }
};

const clearMessages = () => {
    return {
        type: CLEAR_ALL_MESSAGES,
    }
};

const resetPasswordFailure = (error) => {
    return {
        type: RESET_PASSWORD_FAILURE,
        payload: error,
        messages: 'Такого аккаунта не существует!',
    }
};

const userLogout = () => {
    return {
        type: LOGOUT_REQUEST,
    }
};

const userRequest = () => {
    return {
        type: LOGOUT_SUCCESS,
    }
};

const logoutError = (error) => {
    return {
        type: LOGIN_FAILURE,
        payload: error,
    }
};

const clearAllMessages = (tspServices, dispatch) => () => {
    dispatch(clearMessages());
};

const loginUser = (tspServices, dispatch) => (email, password, history) => {
    dispatch(loginRequested());
    tspServices.login(email, password)
        .then(() => dispatch(loginLoaded()))
        .then(() => history.push('/test'))
        .catch((err) => dispatch(loginError(err.response.data)));
};

const loginWithSocial = (tspServices, dispatch) => (id, route, history) => {
    dispatch(loginSocialRequest());
    tspServices.loginSocial(id, route, history)
        .then(() => dispatch(loginSocialLoaded()))
        .catch(err => dispatch(loginSocialError(err)));
};

const resetUserPassword = (tspServices, dispatch) => (email, history) => {
    dispatch(resetPasswordRequest());
    tspServices.resetPassword(email, history)
        .then(() => dispatch(resetPasswordLoaded()))
        .catch(err => dispatch(resetPasswordFailure(err)))
};

const logoutUser = (tspServices, dispatch) => (history) => {
    dispatch(userRequest());
    tspServices.logout()
        .then(() => dispatch(userLogout()))
        .then(() => history.push('/login'))
        .catch(err => {
            dispatch(logoutError(err.response.data))
        });
};

export {
    loginUser,
    loginWithSocial,
    logoutUser,
    resetUserPassword,
    clearAllMessages
}