import React from 'react';
import './spinner.css';
const Spinner = () => {
    return (
        <div className="spinner-wrapper">
            <div className="spinner-custom">
                <div className="spinner-border text-primary" style={{width: '6rem', height: '6rem'}}
                     role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    );
};

export default Spinner;