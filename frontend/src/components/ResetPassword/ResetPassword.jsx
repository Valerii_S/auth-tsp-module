import React, {Component} from 'react';
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import compose from "../../utils/compose";
import {resetUserPassword, clearAllMessages} from "../../actions";
import {withTspServices} from "../Hoc";

class ResetPassword extends Component {
    state = {
        email: '',
        timer: 10,
    };

    // componentDidMount() {
    //     if(this.props.messages) {
    //         setTimeout(() => this.props.clearAllMessages(), 3000)
    //     }
    // }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.messagesSuccess !== this.props.messagesSuccess) {
            setTimeout(() => {
                this.props.history.push('/login');
                this.props.clearAllMessages();
            }, 10000);
            this.timerID = setInterval(() => {
                this.setState({
                    ...this.state,
                    timer: this.state.timer - 1
                });
            }, 1000);
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    handlerEmail = (event) => {
        this.setState({
            email: event.target.value,
        });
    };

    handlerSubmit = (event) => {
        event.preventDefault();
        this.props.resetUserPassword(this.state.email);
    };

    render() {
        const {messages, messagesSuccess, loading} = this.props;
        if (messagesSuccess) {
            return (
                <>
                    <div className="col-6 offset-3 mt-3">
                        <div className="alert alert-success">
                            <p>{messagesSuccess}</p>
                            <p>Ну и прочая дичь которая пишеться в этих случаях</p>
                            <p>Вы будете перенаправлены через
                                <strong> {this.state.timer} </strong> на страницу
                                <Link to="/login" onClick={() => this.props.clearAllMessages()}>логина</Link></p>
                        </div>
                    </div>
                </>
            )
        }
        if (messages) {
            setTimeout(() => this.props.clearAllMessages(), 3000)
        }
        return (
            <div className="col-4 offset-4 mt-5">
                <h1 className="mb-5">Забыли пароль?</h1>
                <form onSubmit={this.handlerSubmit}>
                    <div className="form-group">
                        <input
                            type="email" placeholder="Email"
                            className="form-control"
                            autoComplete="email"
                            onChange={this.handlerEmail}
                            required
                        />
                        { loading && (
                            <div className="spinner-border" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                        )}
                        {messages && (<div className="alert alert-danger mt-2" role="alert">{messages}</div>)}
                    </div>
                    <div className="d-flex justify-content-between">
                    <Link
                        className="btn btn-primary"
                        to={this.props.location.state.login}>
                        Назад
                    </Link>
                    <button type="submit" className="btn btn-primary">Сбросить пароль</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = ({userLogin: {loading, error, messages, messagesSuccess}}) => {
    return {
        loading, error, messages, messagesSuccess
    }
};

const mapDispatchToProps = (dispatch, {tspServices}) => {
    return {
        resetUserPassword: resetUserPassword(tspServices, dispatch),
        clearAllMessages: clearAllMessages(tspServices, dispatch),
    };
};

export default compose(
    withTspServices(),
    connect(mapStateToProps, mapDispatchToProps)
)(withRouter(ResetPassword));