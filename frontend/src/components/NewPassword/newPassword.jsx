import React, {Component} from 'react';
import axios from 'axios';
import {withRouter} from "react-router-dom";

class NewPassword extends Component {
    state = {
        password: '',
        showPassword: false,
    };

    handlerShowPassword = () => {
        this.setState({
            showPassword: !this.state.showPassword,
        });
    };

    handlerUserPassword = (event) => {
        this.setState({
            password: event.target.value
        });
    };

    handlerSubmit = (event) => {
        event.preventDefault();
        const {token} = this.props.match.params;
        axios.post('http://localhost:9000/login/reset/password', {
            _id: localStorage.getItem('id'),
            password: this.state.password,
            token: token,
        })
            .then(() => localStorage.removeItem('id'))
            .then(() => this.props.history.push('/login'))
    };

    render() {
        const {showPassword, password} = this.state;

        return (
            <div>
                <div className="col-4 offset-4 mt-5">
                    <h3 className="mb-5">Введите новый пароль!</h3>
                    <form onSubmit={this.handlerSubmit}>
                        <div className="form-group position-relative d-flex justify-content-end">
                            <input
                                type={showPassword ? 'text' : 'password'}
                                className="form-control"
                                placeholder="Password"
                                autoComplete="current-password"
                                value={password}
                                onChange={this.handlerUserPassword}
                                required
                            />
                            <button
                                type="button"
                                className="btn position-absolute"
                                onClick={this.handlerShowPassword}>
                                {showPassword ? (<i className="fas fa-eye-slash"> </i>) :
                                    (<i className="fas fa-eye"> </i>)
                                }
                            </button>
                        </div>
                        <button type="submit" className="btn btn-primary">Изменить пароль</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default withRouter(NewPassword);