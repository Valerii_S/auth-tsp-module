import React, {Component} from 'react';
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import FacebookLogin from 'react-facebook-login'
import GoogleLogin from "react-google-login";

import {clearAllMessages, loginUser, loginWithSocial} from "../../actions";
import {withTspServices} from "../Hoc";
import compose from "../../utils/compose";
import Spinner from "../Spinner/Spinner";
import './Login.css';

class Login extends Component {
    state = {
        email: null,
        password: null,
        showPassword: false,
        submit: false,
    };

    componentDidMount() {
        const jwt = localStorage.getItem('userHash');
        if (jwt) {
            this.props.history.push('/test');
        }
    }

    dataToState = (stateField) => {
        return (event) => {
            const value = event.target.value;
            this.setState({...this.state, [stateField]: value})
        }
    };

    handlerShowPassword = () => this.setState({
        showPassword: !this.state.showPassword
    });

    handlerSubmit = (event) => {
        event.preventDefault();
        this.setState({submit: true});
        const {email, password} = this.state;
        if (email && password) {
            this.props.loginUser(email, password, this.props.history);
        }
    };

    responseFacebook = (response) => {
        this.props.loginWithSocial(response.id, 'facebook', this.props.history);
    };

    responseGoogle = (response) => {
        this.props.loginWithSocial(response.profileObj.googleId, 'google', this.props.history);
    };

    render() {
        const {showPassword, email, password, submit} = this.state;
        const {loading, messages} = this.props;
        // if (messages) {
        //     setTimeout(() => this.props.clearAllMessages(), 5000)
        // }
        if (this.props.location.login) {
            this.props.clearAllMessages();
        }
        return (
            <>
                {loading && <Spinner/>}
                <div className="col-4 offset-4 mt-5 px-5 py-5 bg-light">
                    <form onSubmit={this.handlerSubmit} noValidate>
                        <div className="form-group">
                            <label htmlFor="email" className="d-none">Email</label>
                            <input
                                type="email" placeholder="Email"
                                id="email"
                                className={"form-control" + (submit && !email ? ' has-error' : '')}
                                onChange={this.dataToState('email')}
                                autoComplete="email"
                            />
                            {submit && !email &&
                            <div className="helper-text text-danger">Email не может быть пустым</div>
                            }
                        </div>
                        <div className="form-group">
                            <label htmlFor="password" className="d-none">Password</label>
                            <div className="position-relative">
                                <input
                                    type={showPassword ? 'text' : 'password'}
                                    className={"form-control" + (submit && !password ? ' has-error' : '')}
                                    placeholder="Password"
                                    id="password"
                                    autoComplete="current-password"
                                    onChange={this.dataToState('password')}
                                />
                                <button
                                    type="button"
                                    className="btn btn-show-password"
                                    onClick={this.handlerShowPassword}>
                                    {showPassword ? (<i className="fas fa-eye-slash"> </i>) :
                                        (<i className="fas fa-eye"> </i>)
                                    }
                                </button>
                            </div>
                            {submit && !password &&
                            <div className="helper-text text-danger">Пароль не может быть пустым</div>
                            }
                        </div>
                        {messages ? (<div className="alert alert-danger" role="alert">{messages}</div>) : null}
                        <p>Не зарегистрированы? <Link to="/register">Регистрация</Link></p>
                        <div className="mt-3">
                            <button type="submit" className="btn btn-primary">Войти</button>
                        </div>
                        <div className="mt-3">
                            <Link to={{
                                pathname: "/login/reset",
                                state: { login: this.props.location }
                            }}
                                  className="btn btn-warning">Забыли пароль?</Link>
                        </div>
                        <div className="mt-3">
                            <p>Или войти с помощью социальных сетей?</p>
                            <FacebookLogin
                                appId="2767052619993496"
                                fields="name, email, picture"
                                callback={this.responseFacebook}/>
                            <GoogleLogin
                                clientId="643479926946-ka4dgns16khhkpjec5ejj2rvgqe012lc.apps.googleusercontent.com"
                                buttonText="Login"
                                onSuccess={this.responseGoogle}
                                onFailure={this.responseGoogle}
                            />
                        </div>
                    </form>
                </div>
            </>
        );
    }
}

const mapStateToProps = ({userLogin: {loading, error, messages}}) => {
    return {loading, error, messages}
};

const mapDispatchToProps = (dispatch, {tspServices}) => {
    return {
        loginUser: loginUser(tspServices, dispatch),
        loginWithSocial: loginWithSocial(tspServices, dispatch),
        clearAllMessages: clearAllMessages(tspServices, dispatch)
    }
};

export default compose(
    withTspServices(),
    connect(mapStateToProps, mapDispatchToProps)
)(withRouter(Login));


