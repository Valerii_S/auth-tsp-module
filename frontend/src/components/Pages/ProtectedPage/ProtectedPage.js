import React from 'react';
import {Component} from "react";
import axios from "axios";
import {withJwtAuth, withTspServices} from "../../Hoc";
import {withRouter} from "react-router-dom";
import {compose} from "redux";
import {connect} from "react-redux";
import {logoutUser} from "../../../actions";

class ProtectedPage extends Component {
    componentDidMount() {
        axios.get('http://localhost:9000/test/', {
            headers: {Authorization: localStorage.getItem('userHash')}
        }).then((response) => this.setState({
            user: response.data,
        }))
            .catch((error) => {
                localStorage.removeItem('userHash');
                this.props.history.push('/login');
                console.error(error);
            });
    }

    handleLogout = () => {
        this.props.logoutUser(this.props.history)
    };

    render() {
        return (
            <div>
                <h1>I am protected!</h1>
                <button onClick={this.handleLogout}>Logout</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

const mapDispatchToProps = (dispatch, { tspServices }) => {
    return {
        logoutUser: logoutUser(tspServices, dispatch),
    }
};

export default compose(withTspServices(), withJwtAuth(), connect(mapStateToProps, mapDispatchToProps))(withRouter(ProtectedPage));