import LoginPage from "./LoginPage/LoginPage";
import ResetPasswordPage from "./Reset/ResetPasswordPage/ResetPasswordPage";
import NewPasswordPage from "./Reset/NewPasswordPage";
import ProtectedPage from "./ProtectedPage";

export {
    LoginPage,
    ResetPasswordPage,
    NewPasswordPage,
    ProtectedPage
}