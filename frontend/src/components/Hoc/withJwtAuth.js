import React, {Component} from 'react';

const withJwtAuth = () => (Wrapped) => {
    return class extends Component {
        componentDidMount() {
            const jwt = localStorage.getItem('userHash');
            if (!jwt) {
                this.props.history.push('/login');
            }
        }

        render() {
            return (
                <>
                    <Wrapped {...this.props} />
                </>
            );
        }
    }
};

export default withJwtAuth;