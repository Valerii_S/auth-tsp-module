import React from 'react';
import {TspServiceConsumer} from "../TspServicesContext";

const withTspServices = () => (Wrapped) => {
    return (props) => {
        return (
            <TspServiceConsumer>
                {
                    (tspServices) => {
                        return (
                            <Wrapped {...props} tspServices={tspServices}/>
                        );
                    }
                }
            </TspServiceConsumer>
        );
    };
};

export default withTspServices;