import React from 'react';

const {Provider: TspServiceProvider, Consumer: TspServiceConsumer} = React.createContext();

export {
    TspServiceProvider,
    TspServiceConsumer
}