import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";

import App from './components/App';
import {Provider} from "react-redux";

import TspServices from "./services/TspServices";

import {TspServiceProvider} from "./components/TspServicesContext";
import store from './store';
const tspServices = new TspServices();


ReactDOM.render(
    <Provider store={store}>
        <TspServiceProvider value={tspServices}>
            <BrowserRouter>
                <App/>
            </BrowserRouter>
        </TspServiceProvider>
    </Provider>,
    document.getElementById('root')
);
