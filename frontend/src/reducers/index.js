import updateUser from './user';

const reducer = (state, action) => {
    return {
        userLogin: updateUser(state, action),
    }
};

export default reducer;