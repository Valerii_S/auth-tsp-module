import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_SUCCESS,
    LOGOUT_REQUEST,
    LOGIN_WITH_SOCIAL_SUCCESS,
    LOGIN_WITH_SOCIAL_REQUEST,
    LOGIN_WITH_SOCIAL_FAILURE,
    LOGOUT_FAILURE,
    RESET_PASSWORD_REQUEST,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAILURE, CLEAR_ALL_MESSAGES
} from '../actions/ActionNames';

const updateUser = (state, action) => {
    if (typeof state === 'undefined') {
        return {
            messages: null,
            messagesSuccess: null,
            loading: false,
            error: null,
        }
    }
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                messages: null,
                messagesSuccess: null,
                loading: true,
                error: null,
            };
        case LOGIN_REQUEST:
            return {
                messages: null,
                messagesSuccess: null,
                loading: false,
                error: null,
            };
        case LOGIN_FAILURE:
            return {
                messages: action.messages,
                messagesSuccess: null,
                loading: false,
                error: action.payload,
            };
        case LOGIN_WITH_SOCIAL_SUCCESS:
            return {
                messages: null,
                loading: false,
                error: null,
            };
        case LOGIN_WITH_SOCIAL_REQUEST:
            return {
                messages: null,
                messagesSuccess: null,
                loading: false,
                error: null,
            };
        case LOGIN_WITH_SOCIAL_FAILURE:
            return {
                messages: action.messages,
                messagesSuccess: null,
                loading: false,
                error: action.payload,
            };
        case RESET_PASSWORD_REQUEST:
            return {
                messages: null,
                messagesSuccess: action.messagesSuccess,
                loading: false,
                error: null,
            };
        case RESET_PASSWORD_SUCCESS:
            return {
                messages: null,
                messagesSuccess: null,
                loading: true,
                error: null,
            };
        case RESET_PASSWORD_FAILURE:
            return {
                messages: action.messages,
                messagesSuccess: null,
                loading: false,
                error: null,
            };
        case CLEAR_ALL_MESSAGES:
            return {
                messages: null,
                messagesSuccess: null,
                loading: false,
                error: null,
            };
        case LOGOUT_SUCCESS:
            return {
                messages: null,
                messagesSuccess: null,
                loading: true,
                error: null,
            };
        case LOGOUT_REQUEST:
            return {
                messages: null,
                messagesSuccess: null,
                loading: false,
                error: null,
            };
        case LOGOUT_FAILURE:
            return {
                messages: null,
                messagesSuccess: null,
                loading: false,
                error: action.payload,
            };
        default:
            return state.userLogin;
    }
};

export default updateUser;
