const User = require('../../schemas/user');
const jwt = require('jsonwebtoken');
const secret = process.env.SECRET || 'some other secret as default';

module.exports = (app) => {
    app.post('/login/facebook', async (req, res) => {
        try {
            const user = await User.findOne({ 'facebook.id': req.body.id });
            const error = {};

            if (!user) {
                error.signIn = 'user not found!';
                res.status(404).send(error);
            }

            const payload = {
                id: user._id,
                email: user.email,
            };

            jwt.sign(payload, secret, { expiresIn: 3600 }, (err, token) => {
                if (err) {
                    res.status(500).send({
                        error: 'Error signing token',
                        raw: err,
                    });
                }
                res.send({
                    success: true,
                    token: `Bearer ${token}`,
                });
            });
        } catch (error) {
            console.error(error);
        }
    });
};
