const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../../schemas/user');

const secret = process.env.SECRET || 'some other secret as default';

module.exports = (app, passport) => {
    app.post('/login/', async (req, res) => {
        try {
            const {email, password} = req.body;
            const errors = {};
            const user = await User.findOne({ email });
            if (!user) {
                errors.email = 'Account Not Found';
                return res.status(404).send(errors);
            }

            const isMatch = await bcrypt.compare(password, user.password);

            if (isMatch) {
                const payload = {
                    id: user._id,
                    email: user.email,
                };

                jwt.sign(payload, secret, {expiresIn: 3600}, (err, token) => {
                    if (err) {
                        res.status(500).send({
                            error: 'Error signing token',
                            raw: err,
                        });
                    }
                    res.send({
                        success: true,
                        token: `Bearer ${token}`,
                        email: user.email,
                        userInfo: user,
                    });
                });

            } else {
                errors.password = 'Password is incorrect';
                res.status(400).json(errors);
            }

        } catch (error) {
            console.error(error);
        }
    });

    app.get('/test', passport.authenticate('jwt', {session: false}), (req, res) => {
        res.send(req.user);
    });

    app.get('/logout', (req, res) => {
        req.logout();
        res.status(200).send({
            success: true,
        });
    });
};
