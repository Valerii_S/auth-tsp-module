const User = require('../../schemas/user');
const bcrypt = require('bcryptjs');

module.exports = (app) => {
    app.post('/login/reset/password', async (req, res) => {
        try {
            const user = await User.findOne({
                _id: req.body._id,
                resetToken: req.body.token,
                resetTokenExp: {$gt: Date.now()},
            });
            if (user) {
                user.password = await bcrypt.hash(req.body.password, 10);
                user.resetToken = undefined;
                user.resetTokenExp = undefined;
                await user.save();
                res.status(200).send('complete');
            } else {
                res.status(404).send('Not found!');
            }
        } catch (error) {
            console.error(error)
        }
    });
};
