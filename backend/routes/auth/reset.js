const User = require('../../schemas/user');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const sendgrid = require('nodemailer-sendgrid-transport');
const keys = require('../../keys');
const resetEmail = require('../../email/resetEmail');

const transporter = nodemailer.createTransport(sendgrid({
    auth: {
        api_key: keys.SENDGRID_API_KEY
    }
}));

module.exports = (app) => {
    app.post('/login/reset', (req, res) => {
        try {
            const error = {};
            crypto.randomBytes(32, async (err, buffer) => {
                if (err) {
                    error.dich = 'Что-то не так!'
                    return res.status(401).send(error);
                }

                const token = buffer.toString('hex');
                const candidate = await User.findOne({email: req.body.email});

                if (candidate) {
                    candidate.resetToken = token;
                    candidate.resetTokenExp = Date.now() + 60 * 60 * 1000;
                    await candidate.save();
                    await transporter.sendMail(resetEmail(candidate.email, token));
                    res.status(200).json(candidate._id);
                } else {
                    res.status(404).send('Account not found!');
                }
            })
        } catch (error) {
            console.error(error);
        }
    });
};