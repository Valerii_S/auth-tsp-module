const {Schema, model} = require('mongoose');

const userSchema = new Schema({
    email: {
        type: String,
        required: false,
        unique: true,
        min: 3,
        max: 14,
    },
    password: {
        type: String,
        required: false,
        min: 0,
        max: 20,
    },
    telephone: {
        type: String,
        required: false,
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String,
    },
    google: {
        id: String,
        email: String,
        name: String,
    },
    cart: {
        items: [],
    },
    resetToken: String,
    resetTokenExp: Date,
});

module.exports = model('User', userSchema);
