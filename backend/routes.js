const login = require('./routes/auth/login');
const reset = require('./routes/auth/reset');
const password = require('./routes/auth/password');
const facebook = require('./routes/auth/facebook-login');
const google = require('./routes/auth/google-login');

module.exports = (app, passport) => {
    login(app, passport);
    reset(app);
    password(app);
    facebook(app);
    google(app);
};
