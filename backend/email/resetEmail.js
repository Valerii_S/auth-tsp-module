module.exports  = function (email, token) {
    return {
        to: email,
        from: "nodejs-tsp@info",
        subject: 'Востановление доступа',
        html: `
        <h1>Вы забыли пароль?</h1>
        <p>если нет проигнорируйте это письмо</p>
        <p>http://localhost:3000/login/reset/password/${token}</p>
        `
    };
};