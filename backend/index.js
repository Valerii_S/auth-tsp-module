const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const passport = require('passport');
require('./config/passport')(passport);
const routes = require('./routes');

const app = express();

app.use(bodyParser.json());
app.use(cors());

routes(app, passport);

const PORT = process.env.PORT || 9000;
const MONGODB_URI = 'mongodb://@localhost:27017/tsp-face';

async function start() {
    try {
        await mongoose.connect(MONGODB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        }, () => {
            console.log('DATABASE READY')
        });
        app.listen(PORT, () => {
            console.log(`SERVER IS RUNNING ON PORT ${PORT}`);
        });
    } catch (error) {
        console.error(error);
    }
}

start();