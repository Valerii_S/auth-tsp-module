const jwtStrategy = require('./jwt');

module.exports = function (passport) {
    passport.use(jwtStrategy());
};
