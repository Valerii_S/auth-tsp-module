const {Strategy, ExtractJwt} = require('passport-jwt');
const User = require('../../schemas/user');
const secret = process.env.SECRET || 'some other secret as default';

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: secret,
};

module.exports = () => {
    return new Strategy(opts, async (payload, done) => {
        try {
            const user = await User.findById(payload.id);
            if (user) {
                return done(null, user);
            } else {
                return done(null, false)
            }
        } catch (error) {
            console.error(error);
        }
    });
};